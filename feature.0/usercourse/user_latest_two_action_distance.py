#!/usr/bin/pypy
# -*- coding: utf-8 -*-
from __future__ import division
import sys, argparse
import csv
import time as t
import datetime as d
'''
用户在当前课程上两个操作间的时间距离
'''

EMPTY_ACTION = 0



def parse_args():
    if len(sys.argv) == 1:
        sys.argv.append('-h')

    parser = argparse.ArgumentParser()
    parser.add_argument('log')
    parser.add_argument('output')
    args = vars(parser.parse_args())
    return args

args = parse_args()
log, output = args['log'], args['output']



time_format = "%Y-%m-%dT%H:%M:%S"

user_action = {}

line_no = 0
reader = csv.reader(open(log))
for enrollment_id,username,course_id,time,source,event,_object in reader:
    line_no += 1 
    if line_no == 1: continue
    _time = t.strptime(time, time_format)

    date = d.datetime(_time.tm_year, _time.tm_mon, _time.tm_mday, _time.tm_hour, _time.tm_min, _time.tm_sec)
    #key = "%s-%s" % (username, course_id)
    key = enrollment_id
    if key not in user_action:
        user_action[key] = {
            'overall': []
            }
    rcd = user_action[key]
    if event not in rcd:
        rcd[event] = []
    rcd[event].append(date)
    rcd['overall'].append(date)

# get feas
writer = csv.writer(open(output, 'w'))
writer.writerow(["enrollment_id", "num_actions", "overall", 'problem', 'video', 'access', 'wiki', 'discussion', 'nagivate', 'page_close'])

for key, rcd in user_action.items(): 
    res = []
    res.append(len(rcd['overall']))
    for action in ["overall", 'problem', 'video', 'access', 'wiki', 'discussion', 'nagivate', 'page_close']:
        # 如果没有记录 设为0
        if action not in rcd or  \
            len(rcd[action]) < 2:
            res.append(EMPTY_ACTION)
            continue
        r = rcd[action]
        r = sorted(r)
        n_hours = (r[-1] - r[-2]).total_seconds() / 3600
        res.append(n_hours)
    assert(len(res) == 9)
    writer.writerow([key] + res)

