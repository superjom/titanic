#!/home/chunwei/chunenv/bin/python
# -*- coding: utf-8 -*-
import sys
import pandas as pd
import numpy as np
import argparse
'''
初始化user的feature

加入username
'''
def parse_args():
    if len(sys.argv) == 1:
        sys.argv.append('-h')

    parser = argparse.ArgumentParser()
    parser.add_argument('log_path')
    parser.add_argument('user_fea_csv')
    args = vars(parser.parse_args())
    return args

args = parse_args()
log_path = args['log_path']
user_fea_csv = args['user_fea_csv']


log = pd.read_csv(log_path)

userfea = pd.DataFrame()

usernames = set()

log.username.map(lambda x: usernames.add(x))
userfea['username'] = list(usernames)
userfea = userfea.set_index('username')
userfea.to_csv(user_fea_csv)
