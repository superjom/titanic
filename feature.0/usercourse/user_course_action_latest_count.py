#!/usr/bin/pypy
# -*- coding: utf-8 -*-
from __future__ import division
import sys, argparse
import csv
import time as t
import datetime as d
'''
用户在当前课程上最近5天，10天的action数目
'''

EMPTY_ACTION = 0


def parse_args():
    if len(sys.argv) == 1:
        sys.argv.append('-h')

    parser = argparse.ArgumentParser()
    parser.add_argument('n_days')
    parser.add_argument('log')
    parser.add_argument('output')
    args = vars(parser.parse_args())
    return args

args = parse_args()
n_days, log, output = args['n_days'], args['log'], args['output']



time_format = "%Y-%m-%dT%H:%M:%S"


# 统计用户action的种类和时间
user_action = {}

line_no = 0
reader = csv.reader(open(log))
for enrollment_id,username,course_id,time,source,event,_object in reader:
    line_no += 1 
    if line_no == 1: continue
    _time = t.strptime(time, time_format)

    date = d.datetime(_time.tm_year, _time.tm_mon, _time.tm_mday, _time.tm_hour, _time.tm_min, _time.tm_sec)
    #key = "%s-%s" % (username, course_id)
    key = enrollment_id
    if key not in user_action:
        user_action[key] = {
            'overall': []
            }
    rcd = user_action[key]
    if event not in rcd:
        rcd[event] = []
    rcd[event].append(date)
    rcd['overall'].append(date)


def action_count_within_latest_n_days(rcd, n_days):
    latest_date = sorted(rcd['overall'])[-1]
    res = []
    for action in ["overall", 'problem', 'video', 'access', 'wiki', 'discussion', 'nagivate', 'page_close']:
        date_list = []
        num_actions = EMPTY_ACTION
        if action in rcd:
            date_list = sorted(rcd[action])
            for date in date_list:
                if (latest_date - date).days <= n_days:
                    num_actions += 1
        res.append(num_actions)
    return res


writer = csv.writer(open(output, 'w'))

# write header
header = ["enrollment_id", "overall", "problem", "video", "access", "wiki", "discussion", "nagivate", "page_close"]
writer.writerow(header)

for key, rcd in user_action.items(): 
    res = action_count_within_latest_n_days(rcd, n_days)
    assert(len(res) == 8)
    assert(res[0] > 0)
    writer.writerow([key] + res)
