#!/home/chunwei/chunenv/bin/python
# -*- coding: utf-8 -*-
import sys
import pandas as pd
import numpy as np
import datetime as d
import argparse
'''
基于 LOG 创建 enrollment
'''

def parse_args():
    if len(sys.argv) == 1:
        sys.argv.append('-h')

    parser = argparse.ArgumentParser()
    parser.add_argument('log')
    parser.add_argument('enrollment')
    parser.add_argument('output')
    args = vars(parser.parse_args())
    return args

args = parse_args()
log = args['log']
enrollment_path = args['enrollment']
output = args['output']




enrollment_ids = set()
log = pd.read_csv(log)
log.enrollment_id.map( lambda x: enrollment_ids.add(x))
enrollment = pd.read_csv(enrollment_path)
enrollment = enrollment.set_index('enrollment_id')

out_data = pd.DataFrame()
out_data['enrollment_id'] = list(enrollment_ids)
out_data = out_data.set_index('enrollment_id')

out_data = pd.concat([out_data, enrollment], join='inner', axis=1)

print 'output to ', output
out_data.to_csv(output)
