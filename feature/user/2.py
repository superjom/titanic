# coding: utf-8
import pandas as pd
import numpy as np
import datetime as d
log = pd.read_csv('../../dataset/log_train.csv')
log.columns
event_set = set()
log.event.map(lambda x: event_set.add(x))
event_set
log_date = log.time.map(lambda x: d.datetime.strptime(x, "%Y-%m-%dT%H:%M:%S"))
log['date'] = log_date
log.columns
user_event_date = {}
for username, event, date in log[ ["username", "event", "date"] ].values:
    if username not in user_event_date:
        user_event_date[username] = { 'overall': [] }
     
get_ipython().magic(u'save 2.py 1-14')
