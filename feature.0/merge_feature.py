#!/usr/bin/python
# -*- coding: utf-8 -*-
from __future__ import division
import sys, argparse
import csv
import time as t
import numpy as np
import random
import datetime as d
import pandas as pd
'''
根据传入的enrollment_id，拼合feature
'''

def parse_args():
    if len(sys.argv) == 1:
        sys.argv.append('-h')

    parser = argparse.ArgumentParser()
    parser.add_argument('feas')
    parser.add_argument('output')
    args = vars(parser.parse_args())
    return args

args = parse_args()
feas, output = args['feas'], args['output']

keys = [int(i) for i in sys.stdin.read().split()]

feas = feas.split()

def merge_feas(feas):
    merged_fea = None
    datas = []
    no = 0
    for f in feas:
        no += 1
        data = pd.read_csv(f)
        data.index = data.enrollment_id
        if no > 1:
            data = data.drop('enrollment_id', axis=1)
        datas.append(data)
    merged_fea = pd.concat(datas, axis=1, keys=range(len(datas)))
    return merged_fea

empty_fea = pd.DataFrame(range(len(keys)), index=keys)

final_fea = pd.concat([empty_fea, merge_feas(feas)], axis=1, join='inner')

final_fea = final_fea.drop(0, axis=1)



with open(output, 'w') as f:
    f.write(','.join(['enrollment_id', 'label'] +  [str(i) for i in range(final_fea.shape[1] - 2)]) + '\n')
    for fea in final_fea.values:
        f.write( ','.join([str(i) for i in fea]) + '\n')
